/*
 * YTCNSRV.cxx
 *
 * Copyright 2021  <pi@master>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CSRV_H
#define CSRV_H

#include <iostream>

#include <algorithm>
#include <fcntl.h>
#include <netinet/in.h>
#include <set>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

// #include <linux/can.h>
// #include <linux/can/raw.h>

#include <net/if.h>
#include <string.h>
#include <sys/ioctl.h>
#include <time.h>

// #include "user_bus.h"
// #include "user_bus_hardware.h"

#include "cans.h"
#include "cant.h"

#include <fstream>
#include <iostream>

using namespace std;

#define SERVERPORT 3425

#define TIMESHIFT 946684800 // 01.01.2000 00:00

#define THIS_NODE_BUS_ID 11
#define BROADCAST_NODE_BUS_ID 31

const std::vector<std::string> StrName
    = { "Repeat",   "Flags",      "ID_CRC8", "Reciever_NET",
        "Reciever", "Sender_NET", "Sender",  "Type" };
const std::vector<uint8_t> StrLen = { 1, 5, 8, 2, 5, 2, 5, 1 };
const std::vector<bool> StrKey = { 0, 0, 0, 1, 1, 0, 0, 0 };

// СПЕЦИАЛЬНО СОЗДАННЫЕ ФУНКЦИИ

// bus_frame bff;

// выдается временной маркер (число милисекунд от события)
uint32_t GETTICK()
{

    struct timespec curtm;

    clock_gettime(CLOCK_REALTIME, &curtm);
    // cout<<"Get Tick!!! "<<" nsec "<<curtm.tv_nsec<<endl;
    return (uint32_t)curtm.tv_nsec;

}; // (*getTick)();

uint8_t receiveFRAME(cant *frame)
{

    printf("rec. frame:\n");
    frame->PrintFrame();

    return 0;
};

//void FrameToArray(uint8_t *buffer, cant *frame)
void FrameToArray(uint32_t *buffer, cant *frame)
{
    for(int i=0;i<41;i++)
                 buffer[i]=0;


    // преобразуем фрейм в массив целых чисел
    buffer[0] = (uint32_t)'C';
    buffer[1] = (uint32_t)'A';
    buffer[2] = (uint32_t)'N';

    for(int i=0;i<8;i++)
          buffer[3+i] = frame->frm.data[i];

    buffer[11] = frame->getIdAttrSize();

    for(int i=0;i<buffer[11];i++)
          buffer[12+i] = frame->getAttrByNdx(i);


}; // FrameToArray

void ArrayToFrame(uint32_t *buffer, cant *frame)
{

    if(frame->getIdAttrSize()!=buffer[11])
    {
        printf("csr ERROR: frame->getIdAttrSize()!=buffer[11]");
        exit(101);
    }

    // преобразуем массив целых чисел во фрейм


    for(int i=0;i<8;i++)
          frame->frm.data[i]=buffer[3+i];

    buffer[11] = frame->getIdAttrSize();

    for(int i=0;i<buffer[11];i++)
          frame->setAttrByNdx(i, buffer[12+i]);


}; // ArrayToFrame

uint8_t transmit_frame(int socan, cant frame)
{

    int bytes_send = 0;
    frame.codeId();
    // frame.PrintFrame();

    for (int jj = 0; jj < 10; jj++)
        {

            bytes_send = send(socan, &frame.frm, sizeof(struct can_frame),
                              MSG_DONTWAIT);

            // myfile<<" | bs="<<bytes_send<<endl;

            if (bytes_send < 0)
                {
                    // myfile<<"ERROR CANSEND SERVER!!! "<<jj<<endl;
                    sleep(1);
                }
            else
                {
                    jj = 100;
                };

        }; // for

    return 1;
}; // transmit_frame

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

/////////////////////MAIN////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{

    cout << "Start Simple CAN-TCP NonBlock Server!" << endl;

    cout << "Parameters: ";
    cout << argc << " : ";
    for (int i = 0; i < argc; i++)
        {
            cout << argv[i] << " | ";
        };
    cout << endl;

    string aa = " is my NodeID (Sender NodeID)";

    int MyID = 0;
    int MyNetID = 0;

    if (argc == 1)
        {
            cout << "Start without parameters! ";
            MyID = THIS_NODE_BUS_ID;
        };

    if (argc == 2)
        {
            // string sss;
            // sss=argv[1];
            cout << "Start with 1 parameter! "
                 << endl; // argv[1]<<" sss="<<sss<<endl;

            // MyID=std::stoi(sss);
            MyID = stoi(argv[1]);
        };

    if (argc == 3)
        {
            cout << "Start with 2 parameter! " << endl;
            MyID = stoi(argv[1]);
            MyNetID = stoi(argv[2]);
        };
    if (argc > 3)
        {

            cout << "Start with more than 2 parameters! ";
            MyID = THIS_NODE_BUS_ID;
            exit(999);
        };

    cout << " my NodeID=:" << MyID << " my NetID=:" << MyNetID << endl;

    // CAN +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    int socan; // это собственно переменная handle сокета
    struct sockaddr_can addrcan;
    struct ifreq ifrcan;

    // Прежде всего нужно создать сокет. Функция эта имеет три параметра:
    // PF_CAN – domain/protocol family то есть тип протокола (у нас тут can).
    //  SOCK_RAW - type of socket (raw or datagram) – тип сокета,
    //  CAN_RAW – протокол сокета (у нас тут снова can)
    if ((socan = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
        {
            perror("Socket CAN ERROR!!!");
            exit(101);
        }

    fcntl(socan, F_SETFL, O_NONBLOCK); // Неблокирующий сокет

    cout << "Socket CAN Ok!\n" << endl;

    // Теперь нужно получить индекс интерфейса, который запущен в системе.
    // Обычно эти имена can0, can1, vcan0 .
    //  копируем в Нуль-строку структуры данных имя интерфейса, который у нас
    //  поднят.
    strcpy(ifrcan.ifr_name, "vcan0");
    // Если же использовать НОЛЬ в качестве индекса интерфейса,
    //  то будем получать в открытый сокет все, что приходит
    //  на все запущенные интерфейсы can!
    //  Чтобы получить такой индекс надо сделать контрольный звонок (ioctl) в
    //  требуемый интерфейс и сохранить результат в структуре данных struct
    //  ifreq
    ioctl(socan, SIOCGIFINDEX, &ifrcan);
    // Теперь функцией bind связываем сокет и запущенный интерфейс.
    // Сначала создаем и заполняем структуру данных адреса
    memset(&addrcan, 0, sizeof(addrcan));
    addrcan.can_family = AF_CAN;
    addrcan.can_ifindex = ifrcan.ifr_ifindex;
    // Теперь связываем
    cout << "Bind CAN " << endl;
    if (bind(socan, (struct sockaddr *)&addrcan, sizeof(addrcan)) < 0)
        {
            perror("Bind");
            exit(102);
        }

    cout << "Bind CAN Ok! socan=" << socan << endl;

    // setSOCANUB(socan); // установка переменной socanUB в user_bus_hardware.c

    // CAN +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    cout << "start nonblocking server" << endl;

    int listener;
    struct sockaddr_in addr;
    int bytes_read;

    listener = socket(AF_INET, SOCK_STREAM, 0); // создали сокет
    if (listener < 0)
        {
            perror("IP socket ERROR!!!");
            exit(1);
        }

    cout << "slistener=" << listener << endl;

    fcntl(listener, F_SETFL, O_NONBLOCK); // сделали неблокирующим

    addr.sin_family = AF_INET;
    addr.sin_port = htons(SERVERPORT);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            perror("IP bind ERROR!!!");
            exit(2);
        }

    listen(listener, 2);

    set<int> clients; // множество клиентов
    clients.clear();  // пока пустое

    // ОБЩИЕ ПЕРЕМЕННЫЕ КОНВЕЙЕРА

    struct can_frame frame;

    cant FR(StrName, StrLen, StrKey);

    bool FrameRec = false;

    /*
    cout << "BUS_INIT start" << endl;
    // инициализация шины CAN
    static user_bus_cfg_typedef UBCFG;
    UBCFG.thisNetID = 0;
    UBCFG.thisNodeID = MyID; // THIS_NODE_BUS_ID;//RBP //уточнить!!!
    UBCFG.broadcastNodeID = BROADCAST_NODE_BUS_ID;
    UBCFG.io_timeout = 10000;
    UBCFG.getTick = GETTICK;
    UBCFG.cmd_run = 0;
    // bus_cfg.cmd_run = &cmd_run;

    user_bus_init(&UBCFG); // инициализация шины CAN +++++++++++++++++++++

    // конфигурирование шины в режим работы только с одной физической шиной
    user_bus_set_redundancy_mode(BUS_REDUNDANCY_MODE_AONLY);

    std::cout << "INIT ok!\n";
    */
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    //  ГЛАВНЫЙ КОНВЕЙЕР
    while (1)
        {

            cout << "================ while START! ===============" << endl;

            // Заполняем множество сокетов
            fd_set readset; // запрос на считывание данных
            FD_ZERO(&readset);
            FD_SET(listener, &readset);
            FD_SET(socan, &readset);

            /*

            fd_set writeset;//запрос на запись данных
            FD_ZERO(&writeset);
            FD_SET(listener, &writeset);
            FD_SET(socan, &writeset);

            fd_set errset;//запрос на запись данных
            FD_ZERO(&errset);
            FD_SET(listener, &errset);
            FD_SET(socan, &errset);
         //*/

            for (set<int>::iterator it = clients.begin(); it != clients.end();
                 it++)
                {
                    FD_SET(*it, &readset);
                    // FD_SET(*it, &writeset);
                    // FD_SET(*it, &errset);
                };

            int mx0 = max(listener, socan);
            int mx = max(mx0, *max_element(clients.begin(), clients.end()));
            /*
            cout<<"readset ";
             for(int j=0;j<(mx+1);j++){
             cout<<" s:"<<j<<" r:"<<FD_ISSET(j, &readset);
             };
            cout<<endl;

            cout<<"writeset ";
             for(int j=0;j<(mx+1);j++){
             cout<<" s:"<<j<<" r:"<<FD_ISSET(j, &writeset);
             };
            cout<<endl;
            cout<<"errset ";
             for(int j=0;j<(mx+1);j++){
             cout<<" s:"<<j<<" r:"<<FD_ISSET(j, &errset);
             };
            cout<<endl;
            //*/

            // Задаем таймаут
            timeval timeout;
            timeout.tv_sec = 10;
            timeout.tv_usec = 10000000; // в миллисекундах

            // cout<<"timeout="<<timeout.tv_sec<<endl;

            // Ждем события в одном из сокетов
            cout << "startselect "
                 << " mx=" << mx << endl;
            if (select(mx + 1, &readset, NULL, NULL, &timeout) < 0)
                // if(select(mx+1, &readset, &writeset, &errset, &timeout) < 0)
                {
                    perror("error select");
                    cout << "errno=" << errno << endl;
                    cout << "timeout=" << timeout.tv_sec << endl;
                    if (errno == EWOULDBLOCK)
                        cout << "errno==EWOULDBLOCK" << endl;
                    exit(3);
                }
            // cout<<"endselect! ";
            cout << "readset ";
            for (int j = 0; j < (mx + 1); j++)
                {
                    cout << " s:" << j << " r:" << FD_ISSET(j, &readset);
                };
            cout << endl;
            /*
            cout<<"writeset ";
             for(int j=0;j<(mx+1);j++){
             cout<<" s:"<<j<<" r:"<<FD_ISSET(j, &writeset);
             };
            cout<<endl;
            cout<<"errset ";
             for(int j=0;j<(mx+1);j++){
             cout<<" s:"<<j<<" r:"<<FD_ISSET(j, &errset);
             };
            cout<<endl;
            //*/

            if (FD_ISSET(socan, &readset)) // если получен кадр из шины CAN
                {

                    // Поступили данные от CAN. Читаем их во frame
                    bytes_read
                        = recv(socan, &frame, sizeof(struct can_frame),
                               MSG_DONTWAIT); // recv(*it, buf, 1024, 0);

                    if (bytes_read <= 0)
                        {
                            // Если соединение разорвано удаляем сокет
                            close(socan);
                            cout << "CAN-SOCKET CLOSED!!!" << endl;
                            // continue;
                            exit(400);
                        }

                    cout << "read ok " << bytes_read << "  frame: ";

                    FR.frm = frame;
                    FR.decodeId();
                    cout << " BFF Recieved " << endl;
                    FR.PrintFrame();

                    int NodeI = (int)FR.getAttrByName(
                        "Reciever"); // bff.receiver.nodeID;
                    int NetI = (int)FR.getAttrByName(
                        "Reciever_NET"); // bff.receiver.netID;
                    bool NodePassed;
                    bool NetPassed;

                    NetPassed = (NetI == MyNetID);
                    NodePassed
                        = (NodeI == MyID) || (NodeI == BROADCAST_NODE_BUS_ID);

                    cout << " Net=" << NetI << " Node=" << NodeI
                         << " MyNet=" << MyNetID << " MyNode=" << MyID
                         << " NetPassed=" << NetPassed
                         << " NodePassed=" << NodePassed << endl;

                    if (NetPassed
                        && NodePassed) // если кадр FR пришел по адресу
                        {

                            cout << "ALL Net and Node PASSED !!!" << endl;
                            FrameRec
                                = true; // Кадр FR получен. Начинаем обработку.

                            // запишем

                            // uint8_t sendBuf[41]; // передача осуществляется
                            // как массив целых чисел для кроссплатформенности
                            // клиентов

                            // FrameToArray(sendBuf, &FR); // преобразуем фрейм
                            // в массив целых чисел

                            /*
                               ofstream myfile;
                                myfile.open ("SRXDUMP",fstream::app);

                                for(int k=0;k<41;k++) myfile <<
                            (int)sendBuf[k]<<" ";//myfile

                                myfile<<endl;
                                myfile.close();
                            //*/

                            // receiveFRAME преобразует bff в буферный фрейм
                            // onNewFrameReceiveISR(&bus, receiveFRAME, 0); //
                            // запись в буфер отправка подтверждения или
                            // обработка системной команды шины CAN

                            // ИСПОЛНЕНИЕ ПРЯМЫХ КОМАНД СЕРВЕРУ
                            if ((FR.getAttrByName("Type") == 0)
                                && (FR.getAttrByName("ID_CRC8") == 55))
                                {
                                    // если кадр содержит команду с кодом 55 -
                                    // она обрабатывается на месте и не
                                    // рассылается

                                    //**********************************************COMMAND
                                    // 55********
                                    cout << endl
                                         << "CMD 55: EXECUTING ON SERVER"
                                         << endl;

                                    // квитанция о получении

                                    cant TxFR(StrName, StrLen, StrKey);

                                    TxFR = FR;

                                    ////bus_frame TxF;
                                    ////TxF = bff;

                                    // cout<<"KVITOK 01!!! "<<endl;

                                    TxFR.setAttrByName(
                                        "Reciever_NET",
                                        FR.getAttrByName("Sender_NET"));
                                    TxFR.setAttrByName(
                                        "Reciever",
                                        FR.getAttrByName("Sender"));
                                    TxFR.setAttrByName("Sender_NET", MyNetID);
                                    TxFR.setAttrByName("Sender", MyID);

                                    // TxF.receiver.nodeID = bff.sender.nodeID;
                                    // TxF.receiver.netID = bff.sender.netID;
                                    // TxF.sender.nodeID = MyID;
                                    // TxF.sender.netID = MyNetID;

                                    TxFR.setAttrByName("Type", 0);
                                    TxFR.setAttrByName("ID_CRC8", 47);

                                    // TxF.type = 0;
                                    // TxF.ID_CRC8 = 47;

                                    for (int j = 0; j < 8; j++)
                                        TxFR.frm.data[j] = 0;

                                    TxFR.frm.data[0] = 55;
                                    TxFR.frm.data[1] = 82; //'R'
                                    TxFR.PrintFrame();

                                    // cout<<" Kvitok 01 OK!!! "<<endl;
                                    transmit_frame(
                                        socan, TxFR); // пересылка по шине CAN

                                    // system("sudo chmod +x cmdf");
                                    // system("./cmdf");
                                    char comand[8];
                                    for (int kk = 0; kk < 8; kk++)
                                        comand[kk] = (char)FR.frm.data[kk];
                                    cout << "START COMMAND >" << comand
                                         << endl;
                                    int RZ = 0;
                                    RZ = system(comand);
                                    cout << "STOP COMMAND " << endl << endl;

                                    // cout<<"KVITOK 02!!! "<<endl;

                                    for (int j = 0; j < 8; j++)
                                        TxFR.frm.data[j] = 0;

                                    TxFR.frm.data[0] = 55;
                                    TxFR.frm.data[1] = 82; //'R'
                                    TxFR.frm.data[2] = 79; //'O'
                                    TxFR.frm.data[3] = RZ;
                                    TxFR.PrintFrame();

                                    // cout<<" Kvitok 02 OK!!! "<<endl;
                                    transmit_frame(
                                        socan, TxFR); // пересылка по шине CAN

                                    //**********************************************COMMAND
                                    // 55********
                                }; // if((bff.type==0) && (bff.ID_CRC8==55))

                            if ((FR.getAttrByName("Type") == 0)
                                && (FR.getAttrByName("ID_CRC8") == 0x30))
                                {
                                    // если кадр содержит команду с кодом 0x30
                                    // (48)- она обрабатывается на месте и не
                                    // рассылается

                                    //**********************************************COMMAND
                                    // 0x30********

                                    // квитанция о получении

                                    cant TxFR(StrName, StrLen, StrKey);

                                    TxFR = FR;

                                    ////bus_frame TxF;
                                    ////TxF = bff;

                                    // cout<<"KVITOK 01!!! "<<endl;

                                    TxFR.setAttrByName(
                                        "Reciever_NET",
                                        FR.getAttrByName("Sender_NET"));
                                    TxFR.setAttrByName(
                                        "Reciever",
                                        FR.getAttrByName("Sender"));
                                    TxFR.setAttrByName("Sender_NET", MyNetID);
                                    TxFR.setAttrByName("Sender", MyID);

                                    TxFR.setAttrByName("Type", 0);
                                    TxFR.setAttrByName("ID_CRC8", 47);

                                    for (int j = 0; j < 8; j++)
                                        TxFR.frm.data[j] = 0;

                                    TxFR.frm.data[0] = 48;
                                    TxFR.frm.data[1] = 82; //'R'
                                    TxFR.PrintFrame();

                                    // cout<<" Kvitok 01 OK!!! "<<endl;
                                    transmit_frame(
                                        socan, TxFR); // пересылка по шине CAN

                                    // cout<<endl<<"!!! COMMAND TIME SYNC
                                    // EXECUTING ON SERVER"<<endl;

                                    union
                                    {
                                        uint32_t val;
                                        uint8_t bytes[4];
                                    } b_sec, b_nsec;

                                    b_sec.bytes[0] = FR.frm.data[0];
                                    b_sec.bytes[1] = FR.frm.data[1];
                                    b_sec.bytes[2] = FR.frm.data[2];
                                    b_sec.bytes[3] = FR.frm.data[3];

                                    b_nsec.bytes[0] = FR.frm.data[4];
                                    b_nsec.bytes[1] = FR.frm.data[5];
                                    b_nsec.bytes[2] = FR.frm.data[6];
                                    b_nsec.bytes[3] = FR.frm.data[7];

                                    struct timespec curtm;

                                    curtm.tv_sec = b_sec.val;
                                    curtm.tv_nsec = b_nsec.val;

                                    // clock_settime(CLOCK_REALTIME,&curtm);
                                    // cout<<"CMD 0x30: SET current time
                                    // "<<"sec "<<curtm.tv_sec<<" nsec
                                    // "<<curtm.tv_nsec<<endl;

                                    // int a = (int)b_sec.val;
                                    // char *intStr = itoa(a);
                                    string UTM = to_string(
                                        ((int)b_sec.val + TIMESHIFT));

                                    string CST = "sudo date -s '@" + UTM + "'";
                                    cout << "==>CMD 0x30: SET current time "
                                         << CST << endl;
                                    system(CST.c_str());

                                    cout << "==>CMD 0x30: SET current time "
                                         << UTM << " sec " << endl;

                                    for (int j = 0; j < 8; j++)
                                        TxFR.frm.data[j] = 0;

                                    TxFR.frm.data[0] = 48;
                                    TxFR.frm.data[1] = 82; //'R'
                                    TxFR.frm.data[2] = 79; //'O'

                                    TxFR.PrintFrame();
                                    // cout<<" Kvitok 02 OK!!! "<<endl;
                                    transmit_frame(
                                        socan, TxFR); // пересылка по шине CAN

                                    //**********************************************COMMAND
                                    // 0x30********
                                }; // if((bff.type==0) && (bff.ID_CRC8==0x30))

                        }; // if(NetPassed&&NodePassed){

                }; // if(FD_ISSET(socan, &readset))

            // ОБРАБОТКА ПОДКЛЮЧЕНИЙ КЛИЕНТОВ
            // +++++++++++++++++++++++++++++++++++++

            // Определяем тип события и выполняем соотв. действие
            if (FD_ISSET(listener, &readset))
                {
                    // Если поступил запрос на соединение выполняем accept
                    int sock = accept(listener, NULL, NULL);
                    if (sock < 0)
                        {
                            perror("error accept");
                            exit(3);
                        }

                    fcntl(sock, F_SETFL, O_NONBLOCK);

                    cout << "CLIENT " << sock << " ADDED!!!" << endl;
                    cout << "Listener=" << listener << " I NEW clients set: ";
                    for (const auto &i : clients)
                        cout << i << " ; ";
                    cout << endl;
                    // Добавление сокета клиента
                    clients.insert(sock);
                } // if(FD_ISSET(listener, &readset))

            cout << "Clients size = " << clients.size() << endl;
            // ОБРАБОТКА ПОДКЛЮЧЕНИЙ КЛИЕНТОВ
            // +++++++++++++++++++++++++++++++++++++

            // initNframesToClient();
            //  printf("initNframesToClient %d frames
            //  \n",getNframesToClient());
            // reliable_bus_rx_thread();

            // printf("after user_bus_rx_thread %d frames
            // \n",getNframesToClient()); bus_frame ftc=GetFrameToClient(0);
            // PrintFrames(&ftc);

            if (clients.size() > 0)
                {

                    for (set<int>::iterator it = clients.begin();
                         it != clients.end(); it++)
                        {

                            // проверка живых клиентов
                            if (FD_ISSET(*it, &readset))
                                {

                                    cout << "Client #" << *it
                                         << " IN READSET ";

                                    int SR = 0;
                                    // bus_frame TxF;

                                    cant TxFR(StrName, StrLen, StrKey);

                                    // SR=recv(*it, &TxF,
                                    // sizeof(bus_frame),MSG_DONTWAIT);//вариант
                                    // с передачей фреймов как структур

                                    uint32_t
                                        clientFrameBuf[41]; // принимаем в
                                                            // формате массива
                                                            // целых чисел
                                    SR = recv(*it, clientFrameBuf, 41*sizeof(uint32_t),
                                              MSG_DONTWAIT);

                                    ArrayToFrame(clientFrameBuf,
                                                 &TxFR); // переводим во фреймы

                                    cout << "SendRez=" << SR << endl;

                                    if (SR == 0)
                                        {
                                            close(*it);
                                            clients.erase(*it);
                                        }; // if(SR<0){

                                    if (SR > 0)
                                        {
                                            cout << "SR>0 !!!"; //<<endl;
                                            // PrintFrames(&TxF);

                                            // ВНИМАНИЕ!!! Здесь все, что
                                            // исходит от данного сервера
                                            // получает его адрес как
                                            // отправителя!!!!
                                            TxFR.setAttrByName("Sender", MyID);
                                            TxFR.setAttrByName("Sender_NET",
                                                               MyNetID);

                                            // cout<<"modify !!!"<<endl;
                                            TxFR.codeId();
                                            TxFR.PrintFrame();
                                            cout << "RX FROM CLIENT OK!!!"
                                                 << endl;
                                            transmit_frame(
                                                socan,
                                                TxFR); // пересылка по шине CAN
                                        };

                                }; // if(FD_ISSET(*it, &readset))
                        };         // for it

                    // РАССЫЛКА ПРИНЯТОГО СОКЕТАМ+++++++++++++++++++++++++++++++++++++++
                    cout << "TRANSMISSION TO Clients" << endl;

                    // bus_frame ftc; // структура данных
                    // cant FRtc(StrName, StrLen, StrKey);

                    // int NN = 1;//getNframesToClient(); // получаем число кадров в буфере

                    // for (int j = 0; j < NN; j++){
                    if (FrameRec)
                        {

                            // FRtc = FR;//GetFrameToClient(j); // берем кадр
                            // из буфера
                            FR.PrintFrame();
                            // cout<<"ftc.type="<<ftc.type<<"
                            // ftc.ID_CRC8="<<ftc.ID_CRC8<<endl;

                            bool CODE55
                                = ((FR.getAttrByName("Type") == 0)
                                   && (FR.getAttrByName("ID_CRC8") == 55));
                            bool CODE48
                                = ((FR.getAttrByName("Type") == 0)
                                   && (FR.getAttrByName("ID_CRC8") == 0x30));

                            // if(CODE48||CODE55) {
                            if (CODE55)
                                {
                                    // если кадр содержит команду с кодом 55 -
                                    // она выше уже обработана на месте и не
                                    // рассылается printf("\n CODE48 or CODE55
                                    // \n");
                                }
                            else
                                {
                                    // в противном случае фрейм передается
                                    // клиентам
                                    for (set<int>::iterator it
                                         = clients.begin();
                                         it != clients.end(); it++)
                                        {

                                            // broadcasting bus frame
                                            int sb = 0;
                                            uint32_t sendBuf[41]; // передача
                                                      // осуществляется
                                                      // как массив
                                                      // целых чисел для
                                                      // кроссплатформенности
                                                      // клиентов

                                            FrameToArray(
                                                sendBuf,
                                                &FR); // преобразуем фрейм ftc
                                                      // в массив целых чисел
                                            sb = send(*it, sendBuf, 41*sizeof(uint32_t),
                                                      0); // и отправляем

                                            // sb=send(*it, &ftc,
                                            // sizeof(bus_frame),0);

                                            cout << "Frame #"
                                                 << "j"
                                                 << " to Client #" << *it
                                                 << " send " << sb << endl;

                                        }; // for it
                                }; // if((ftc.type==0) && (ftc.ID_CRC8==55))

                            FrameRec
                                = false; // принятый по шин CAN кадр обработан

                        }; // if(FrameRec){      // };         // for j NN

                    // РАССЫЛКА ПРИНЯТОГО
                    // СОКЕТАМ+++++++++++++++++++++++++++++++++++++++
                }
            else
                {
                    cout << "NO Clients " << clients.size() << endl;

                }; // else if

            // user_bus_tx_thread();

            // can

            // КОНЕЦ КОНВЕЙЕРА
        } // while(1)
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////
    ///////////////////////////////////////////////////

    if (close(socan) < 0)
        { // Закрываем сокет в конце штатной работы
            perror("CAN close");
            // return 1;
            exit(400);
        } // if
    printf(" socan closed!\n");

    return 0;
} // int main()
#endif // CSRV_H
