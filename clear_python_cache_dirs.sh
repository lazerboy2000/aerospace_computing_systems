#!/bin/bash
find . -type d -name "*cache*" -not -path "./venv/*" -exec rm -rf {} \;
