import pytest

from config.mission_parameters import (
    CONCENTRATOR_EFFICIENCY,
    CONCENTRATOR_SIZE_X,
    CONCENTRATOR_SIZE_Y,
    SOL_GOAL_B,
)
from cstpu import SUN_CONSTANT
from src.mission_simulation import heat_flux_from_sun

heat_flux_from_sun_test_data = [
    # менять если меняется SOL_GOAL_B
    pytest.param(0.3, [0, 0.6, 0.8], 0, id="angle > 0.98"),
    pytest.param(0.3, [0, 0, 1], 9.22725, id="angle < 0.98"),
    pytest.param(
        1,
        SOL_GOAL_B,
        SUN_CONSTANT
        * CONCENTRATOR_SIZE_X
        * CONCENTRATOR_SIZE_Y
        * CONCENTRATOR_EFFICIENCY,
        id="max heat_flux",
    ),
]


@pytest.mark.parametrize(
    ("sun_visible_part", "e_sun_b", "expected_value"), heat_flux_from_sun_test_data
)
# см. heat_flux_from_sun_test_data если меняется SOL_GOAL_B
def test_heat_flux_from_sun(sun_visible_part, e_sun_b, expected_value):
    # все компоненты посчитанного единичного вектора должны совпадать с соответствующими
    # компонентами ожидаемого единичного вектора
    assert heat_flux_from_sun(sun_visible_part, e_sun_b) >= expected_value
