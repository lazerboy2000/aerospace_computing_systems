# Инструкция по настройке raspberry pi

## Первичная настройка для подключения по ssh

1. Установить программу raspberry pi imager
1. Нажать на кнопку выбора устанавливаемой операционной системы

    ![Кнопка выбора устанавливаемой операционной
    системы](images/rpi_images_select_os_button.png)

1. Выбрать 64-битную версию на основе Debian bullseye без графического интерфейса

    ![Необходимая версия ОС](images/rpi_imager_64_bit_debian_selection.png)

1. В расширенных настройках в целях безопасности разрешить аутентификацию только
   подключение по ssh-ключу, указать логин и пароль WiFi-сети (ноутбук и все устройства
   Raspberry Pi должны находится в одной WiFi-сети), указать пароль администратора для
   дальнейшей установки программ, указать имя, уникальное для каждой платы Raspberry
   если планируется подключать одновременно несколько устройств в одну сеть.

    ![Расширенные настройки](images/rpi_imager_advanced_options.png)

1. Добавить raspberry в файл ~/.ssh/config

    ![Содержимое файла gitconfig](images/ssh_config.png)

1. В Visual Studio Code открыть меню подключения по ssh, например, нажав `Ctrl+Shift+P`
   и начав писать `ssh connect` будет отображен список похожих команд, выбрать
   `Remote-SSH: Connect to Host...`

    ![Вызов меню подключения по SSH в VSCode](images/vsc_connect_to_ssh_host.png)

1. Выбрать настроенный ssh host

    ![Выбор хоста по имени](images/vsc_select_configured_ssh_host.png)

## Настройка CAN-шины

- Запустите bash-скрипт
  [setup_can_step_1_and_reboot.sh](setup_can_step_1_and_reboot.sh).
- Перезагрузите raspberry.
- Запустите bash-скрипт
  [setup_can_step_2_after_reboot.sh](setup_can_step_2_after_reboot.sh).
