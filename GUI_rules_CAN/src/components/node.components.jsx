import React, { useState, Fragment } from 'react';
import ReactFlow, { addEdge, Background, Controls, MiniMap } from 'react-flow-renderer';

const MindNode = () => {
  const [elements, setElements] = useState([]);
  const [name, setName] = useState("");
  const [selectedElements, setSelectedElements] = useState([]);

  const addNode = () => {
    const x = window.innerWidth / 2;
    const y = window.innerHeight / 2;

    setElements(e => e.concat({
      id: (e.length + 1).toString(),
      data: { label: `${name}` },
      position: { x, y }
    }));
  };

  const removeNode = () => {
    if (selectedElements.length > 0) {
      const selectedNodeId = selectedElements[0].id;
      setElements(e => e.filter(el => el.id !== selectedNodeId));
      setSelectedElements([]);
    }
  };

  const onConnect = (params) => setElements(e => addEdge(params, e));

  const onLoad = (reactFlowInstance) => {
    reactFlowInstance.fitView();
  };

  const onSelectionChange = (elements) => {
    setSelectedElements(elements || []);
  };

  const saveJson = () => {
    const jsonContent = JSON.stringify(elements, null, 2);
    const blob = new Blob([jsonContent], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'flow.json';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  };

  return (
    <Fragment>
      <ReactFlow
        elements={elements}
        onLoad={onLoad}
        style={{ width: '100%', height: '90vh' }}
        onConnect={onConnect}
        connectionLineStyle={{ stroke: "#ddd", strokeWidth: 2 }}
        connectionLineType="bezier"
        snapToGrid={true}
        snapGrid={[16, 16]}
        onSelectionChange={onSelectionChange}
      >
        <Background color="#888" gap={16} />
        <MiniMap
          nodeColor={n => {
            if (n.type === 'input') return 'blue';
            return '#FFCC00';
          }}
        />
        <Controls />
      </ReactFlow>

      <div>
        <input
          type="text"
          onChange={e => setName(e.target.value)}
          name="title"
        />
        <button type="button" onClick={addNode}>Add Node</button>
        <button type="button" onClick={removeNode}>Delete Node</button>
        <button type="button" onClick={saveJson}>Save JSON</button>
      </div>
    </Fragment>
  );
};

export default MindNode;




// import React, {useState, Fragment} from 'react';

// import ReactFlow, {addEdge, Background, Controls, MiniMap} from 'react-flow-renderer';

// const initialElements = [
//     {id: '1', type: 'input', data:{label: 'Can0'}, position: {x:0,y:0}}
// ]
// const onLoad = (reactFlowInstance) =>  {
//     reactFlowInstance.fitView();
// }

// const MindNode = () => {

//     const [elements, setElements] = useState(initialElements);
//     const [name, setName] = useState("")
//     const [lastClickPosition, setLastClickPosition] = useState({ x: 0, y: 0 });
    
//     const addNode = () => {
//         setElements(e => e.concat({
//             id: (e.length+1).toString(),
//             data: {label: `${name}`},
//             position: lastClickPosition //#{x: Math.random() * window.innerWidth, y: Math.random() * window.innerHeight}
//         }));
//     };

//     const onConnect = (params) => setElements(e => addEdge(params,e));
    
//     return(
//         <Fragment>
//             <ReactFlow
//             elements={elements}
//             onLoad={onLoad}
//             style={{width:'100%',height: '90vh'}}
//             onConnect = {onConnect}
//             connectionLineStyle={{stroke: "#ddd", strokeWidth: 2}}
//             connectionLineType = "bezier"
//             snapToGrid = {true}
//             snapGrid={[16,16]}
//             >
//                 <Background
//                 color="#888"
//                 gap={16}
//                 />
//                 <MiniMap 
//                 nodeColor={n=>{
//                     if(n.type === 'input') return 'blue';
                    
//                     return '#FFCC00'
//                 }} />
//                 <Controls />
//                 </ReactFlow>

//             <div>
//                 <input type="text"
//                 onChange={e => setName(e.target.value)}
//                 name="title"/>
//                 <button 
//                 type="button"
//                 onClick={addNode}
//                 >Add Node</button>
//             </div>
//         </Fragment>
//     )
// }

// export default MindNode;