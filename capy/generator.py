import time

import can

from cant import cant

print("Start!")

MY_ID = 11
MY_NET = 0

DATA = b"DEADBEEF"

CAN_PROTOCOL_FILENAME = "cantid"

# TODO перенести код ниже в библиотеку cant
# >>>
with open(CAN_PROTOCOL_FILENAME) as protocol_file:
    lines = protocol_file.readlines()

field_names, field_lengths, field_keys = zip(*(line.split() for line in lines))

# TODO почему в pyt.py не используются бинарные строки, а здесь используются?
# field_names = [f_name.encode("ascii") for f_name in field_names]
print("SetID")
cant().SetId(field_names, field_lengths, field_keys)

print("setID ok")
# <<<


print("setAttrByName=", cant().setAttrByName(b"Sender", MY_ID))
print("setAttrByName=", cant().setAttrByName(b"Sender_NET", MY_NET))

print("setAttrByName=", cant().setAttrByName(b"Reciever", 12))
print("setAttrByName=", cant().setAttrByName(b"Reciever_NET", MY_NET))

print("setAttrByName=", cant().setAttrByName(b"Type", 0))
print("setAttrByName=", cant().setAttrByName(b"ID_CRC8", 63))

print("codeId()      ", cant().codeId())


bus = can.Bus(interface="socketcan", channel="vcan0", bitrate=250000)

print("Start Send")

for i in range(10):
    time.sleep(1)
    print("step=", i)
    print("getAdr()    ", cant().getAdr())
    msg = can.Message(arbitration_id=cant().getAdr(), data=DATA, is_extended_id=True)
    try:
        bus.send(msg)
        # print(f"Message sent on {bus.channel_info}")
    except can.CanError:
        print("Message NOT sent")


print("Finish!")
