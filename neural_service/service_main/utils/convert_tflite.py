import argparse
import os

import tensorflow as tf

path_model = os.path.join("weights/bestmodel.h5")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-m",
    "--model",
    help="path to the model",
    default=path_model,
)
args = parser.parse_args()


def convert_model_tf(model_path):
    keras_model = tf.keras.models.load_model(model_path, compile=False)

    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    tflite_model = converter.convert()

    with open("weights/bestmodel.tflite", "wb") as f:
        f.write(tflite_model)


if __name__ == "__main__":
    convert_model_tf(args.model)
