#!/bin/bash

CNT=DD4
VXD=vxc4
VCB=vcan1
VCO=vcan0
REG=6

echo $CNT $VXD $VCB $4
# $CNT - container name
# $VXD - vxcan in docker
# $VCB - can device in host (can0 or vcan0 etc.) Must be started
# $REG - clt program regime
# vxcan1 - this name can in docker

# Если скрипт запускали ранее, то необходимо остановить и удалить соответствующий
# контейнер
# https://stackoverflow.com/a/38225298
docker stop $CNT || true && docker rm $CNT || true

# переходим на уровень выше, чтобы была доступна папка canms
cd ..
docker run -v "/$(pwd)/canms:/home/canms" --rm -it -d --name $CNT ubcanpy
# возвращаемся обратно в начальную директорию
cd -

DPID=$(docker inspect -f '{{ .State.Pid }}' $CNT)
echo ID=$DPID
sudo ip link add $VXD type vxcan peer name vxcan1 netns $DPID
echo link OK

sudo ip link set $VXD up

sudo modprobe can-gw
sudo cangw -A -s $VCB -d $VXD -e
sudo cangw -A -s $VXD -d $VCO -e

echo gate ok
# nsenter - name space enter
sudo nsenter -t $DPID -n ip link set vxcan1 up
echo exec
docker exec -d $CNT bash -c "cd /home/canms && ./clt vxcan1 $REG 15"


unset DPID
unset CNT
unset VXD
unset VCB
unset VCO
unset REG
