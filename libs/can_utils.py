import can
import numpy as np


# %%
def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


# %%
# bus = can.interface.Bus(bustype="socketcan", channel="vxcan0", bitrate=500000)

# https://github.com/christiansandberg/python-can-remote
bus = can.interface.Bus("ws://can_server:54701/", bustype="remote", bitrate=500000)


def custom_str_conversion(arg):
    if type(arg) in (list, np.ndarray):
        return ",".join(str(elem) for elem in arg)
    else:
        return str(arg)


# %%
def pack_args_to_str(*args):
    # если число - добавляем через запятую
    # если список - добавляем его элементы через запятую
    # если строка - добавляем целиком, отделив всю строку запятой
    result_str = ",".join(custom_str_conversion(arg) for arg in args)

    # добавим символы начала ([) и окончания (]) передачи данных
    return "[" + result_str + "]"


def unpack_args_from_str(message: str):
    message = message.strip("[]")
    args = message.split(sep=",")
    # последние 2 элемента - строки, их не надо преобразовывать
    args[:-2] = list(map(float, args[:-2]))
    t = args[0]
    y = args[1:-2]
    cur_sat_state = args[-2]
    strategy = args[-1]

    return t, y, cur_sat_state, strategy


def send_args_to_can_bus(*args, arbitration_id=None):
    data = list(map(ord, pack_args_to_str(*args)))

    # Побьем data на куски по 8 байт
    for data_chunk in chunks(data, 8):
        msg = can.Message(
            arbitration_id=arbitration_id, data=data_chunk, is_extended_id=True
        )

        try:
            bus.send(msg)
            # print(f"Message sent on {bus.channel_info}")
        except can.CanError:
            print("Message NOT sent")
